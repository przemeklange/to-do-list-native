let $todoInput
let $alertInfo
let $addBtn
let $ulList
let $newTask
let $toolsPanel
let $completeBtn
let $editBtn
let $deleteBtn
let $tools
let $popup
let $popupinfo
let $editedTodo
let $popupInput
let $addPopupBtn
let $closeTodoBtn
let $idNumber = 0
let $alltask


const main = () => {
    prepareDOMElements();
    prepareDOMEvents();
}


const prepareDOMElements = () => {
    $todoInput = document.querySelector('.todoInput');
    $alertInfo = document.querySelector('.alertInfo');
    $addBtn = document.querySelector('.addBtn');
    $ulList = document.querySelector('.todoList ul');
    $tools = document.querySelector('.tools');

    $popup = document.querySelector('.popup');
    $popupinfo = document.querySelector('.popupInfo')
    $popupInput = document.querySelector('.popupInput')
    $addPopupBtn = document.querySelector('.accept')
    $closeTodoBtn = document.querySelector('.cancel')
    $alltask = $ulList.querySelectorAll('li')
}


const prepareDOMEvents = () => {
    $addBtn.addEventListener('click', addnewTask)
    $ulList.addEventListener('click', checkClick)
    $closeTodoBtn.addEventListener('click', closePopup)
    $addPopupBtn.addEventListener('click', changeTodo)
    $todoInput.addEventListener('keyup', keyadd)
}

const addnewTask = () => {
    if ($todoInput.value !== "") {
        $idNumber++
        $newTask = document.createElement("li")
        $newTask.innerText = $todoInput.value
        $newTask.setAttribute('id', `todo-${$idNumber}`)
        $ulList.appendChild($newTask)

        $todoInput.value = ""
        $alertInfo.innerText = ''
        createToolsArea()
    } else {
        $alertInfo.innerText = 'Wpisz treść zadania!'
    }
}

const createToolsArea = () => {
    $toolsPanel = document.createElement("div")
    $toolsPanel.classList.add('tools')
    $newTask.appendChild($toolsPanel)

    $completeBtn = document.createElement("button")
    $completeBtn.classList.add('complete')
    $completeBtn.innerHTML = `<i class="fas fa-check"></i>`
    $toolsPanel.appendChild($completeBtn)

    $editBtn = document.createElement('button')
    $editBtn.classList.add('edit')
    $editBtn.innerText = "EDIT"
    $toolsPanel.appendChild($editBtn)

    $deleteBtn = document.createElement("button")
    $deleteBtn.classList.add('delete')
    $deleteBtn.innerHTML = `<i class="fas fa-times"></i>`
    $toolsPanel.appendChild($deleteBtn)
}

const checkClick = (e) => {
    if (e.target.closest('button').classList.contains('complete')) {
        e.target.closest('li').classList.toggle('completed')
        e.target.closest('button').classList.toggle('completed')
    } else if (e.target.closest('button').className === 'edit') {
        editTask(e)
    } else if (e.target.closest('button').classList.contains('delete')) {
        deleteTask(e)
    }
}

const editTask = (e) => {
    const oldTodo = e.target.closest('li').id
    $editedTodo = document.getElementById(oldTodo)
    $popupInput.value = $editedTodo.firstChild.textContent
    $popup.style.display = "flex"
}

const changeTodo = () => {
    if ($popupInput.value !== '') {
        $editedTodo.firstChild.textContent = $popupInput.value
        $popup.style.display = "none"
    }else{
        $popupinfo.innerText = 'Nie można dodać pustego zadania!'
    }
}

const deleteTask = (e) => {
    const deleteTodo = e.target.closest('li')
    deleteTodo.remove()

    if($alltask.length === 0){
        $alertInfo.innerText = 'Wpisz treść zadania!'
    }

}

const keyadd = (event) => {
    if(event.keyCode === 13){
        addnewTask()
    }
}

const closePopup = () => {
    $popup.style.display = "none"
}




document.addEventListener('DOMContentLoaded', main)